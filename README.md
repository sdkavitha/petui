# PetUI

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.2.

## Steps to test this App

Run `npm install` 

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PetComponent } from './pet/pet.component';
import { ApiService } from './services/api.service';
import { PetService } from './pet/pet.service';

@NgModule({
  declarations: [
    AppComponent,
    PetComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [ApiService, PetService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class ApiService {

  protected apiUrl: string;

  
  constructor(public http: HttpClient) {
       this.apiUrl = environment.apiUrl;
  }

  public get<T>(url: string, params?: any): Observable<T> {
    return this.http.get<T>(this.getApiUrl(url), { params: this.buildUrlSearchParams(params) });
  }

  public post<T>(url: string, data?: any, params?: any): Observable<T> {
    return this.http.post<T>(this.getApiUrl(url), data, { params: params });
  }

  public put<T>(url: string, data?: any, params?: any): Observable<T> {
    return this.http.put<T>(this.getApiUrl(url), data, { params: params });
  }

  public delete<T>(url: string): Observable<T> {
    return this.http.delete<T>(this.apiUrl + url);
  }

  private getApiUrl(url: string): string {
    return encodeURI(this.apiUrl + url);
  }

  private buildUrlSearchParams(params: any): HttpParams {
      debugger
    const searchParams = new HttpParams();
    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        searchParams.append(key, params[key]);
      }
    }

    return searchParams;
  }

}



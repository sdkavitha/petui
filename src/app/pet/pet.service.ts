import { Injectable } from '@angular/core';
import { ApiService } from '../services/api.service'
import { Observable } from 'rxjs'

@Injectable()
export class PetService {
    constructor(private apiService: ApiService) {
  }

  getPetDetails():Observable<any> {
    return this.apiService.get('api/Pet/GetPetDetails');
  }
}

import { Component, OnInit, Inject, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PetService } from '../pet/pet.service'

@Component({
  selector: 'app-pet',
  templateUrl: './pet.component.html',
})
export class PetComponent {
  maleOwnerPets: any[] = [];
  femaleOwnerPets: string[] = [];
  loading: boolean;

  constructor(private petService: PetService) {  
  }

  ngOnInit() {
    this.loading = true;
    this.petService.getPetDetails().subscribe(result => {
      this.loading = false;
      this.maleOwnerPets = result.filter(x => x.gender === 'Male');
      this.femaleOwnerPets = result.filter(x => x.gender === 'Female');

    }, error => {
      this.loading = false;
      console.error(error)});

  }


}


